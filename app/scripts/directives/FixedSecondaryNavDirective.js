'use strict';

/*
A simple AngularJS directive to render a smooth scroll effect
Usage: <element fixed-navigation tagline-id="cd-intro-tagline"></element>
*/

angular.module('angularApp').directive('fixedNavigation', [
  '$log', '$timeout', '$window', '$document',
  function($log, $timeout, $window, $document) {
    /*
        Retrieve the current vertical position
        @returns Current vertical position
    */

    var secondaryNav,
      secondaryNavTopPosition,
      taglineEl,
      taglineOffesetTop,
      contentSections;

    var currentYPosition, elmYPosition, smoothScroll, getElement;

    getElement = function(elementId) {
      var el = document.getElementById(elementId);

      return el;
    };

    function updateSecondaryNavigation() {
      var contentSectionsLength = contentSections.length;
      for (var i = 0; i < contentSectionsLength; i++) {
        var actual = contentSections[i];
        var actualHeight = actual.scrollHeight + getStyle(actual, 'padding-top') + getStyle(actual, 'padding-bottom');
        var actualAnchor = secondaryNav.querySelector('a[target-id="' + actual.id + '"]');

        if ((actual.offsetTop - secondaryNav.scrollHeight <= $window.pageYOffset) && (actual.offsetTop + actualHeight - secondaryNav.scrollHeight > $window.pageYOffset)) {
          angular.element(secondaryNav.querySelectorAll('.active')).removeClass('active');
          actualAnchor.classList.add('active');
        } else {
          actualAnchor.classList.remove('active');
        }
      }
    }

    function getStyle(el, styleAttr) {
      var styleValue = window.getComputedStyle(el, null).getPropertyValue(styleAttr).replace('px', '');

      styleValue = parseInt((styleValue * 1));

      return styleValue;
    }
    return {
      restrict: 'A',
      link: function(scope, element, attr) {
        // start from page top
        window.scrollTo(0, 0);
        secondaryNav = angular.element(element)[0];
        secondaryNavTopPosition = secondaryNav.offsetTop;
        taglineEl = getElement('cd-intro-tagline');
        taglineOffesetTop = taglineEl.offsetTop + taglineEl.offsetHeight + getStyle(taglineEl, 'padding-top');
        contentSections = angular.element(document.querySelectorAll('.cd-section'));

        angular.element($window).unbind('scroll');
        angular.element($window).bind('scroll', function() {
          if ($window.pageYOffset > taglineOffesetTop) {
            document.querySelector('#cd-logo').classList.add('is-hidden');
            document.querySelector('.cd-btn').classList.add('is-hidden');
          } else {
            if(document.querySelector('#cd-logo')) {
              document.querySelector('#cd-logo').classList.remove('is-hidden');
              document.querySelector('.cd-btn').classList.remove('is-hidden');
            }
          }

          if ($window.pageYOffset > secondaryNav.offsetTop) {
            //fix secondary navigation
            secondaryNav.classList.add('is-fixed');
            //push the .cd-main-content giving it a top-margin
            document.querySelector('.cd-main-content').classList.add('has-top-margin');
            //on Firefox CSS transition/animation fails when parent element changes position attribute
            //so we to change secondary navigation childrens attributes after having changed its position value
            setTimeout(function() {
              secondaryNav.classList.add('animate-children');
              document.querySelector('#cd-logo').classList.add('slide-in');
              document.querySelector('.cd-btn').classList.add('slide-in');
            }, 50);
          } else {
            if(document.querySelector('.cd-main-content')) {
              secondaryNav.classList.remove('is-fixed');
              document.querySelector('.cd-main-content').classList.remove('has-top-margin');
              var intervalId = setTimeout(function() {
                if(document.querySelector('#cd-logo')) {
                  secondaryNav.classList.remove('animate-children');
                  document.querySelector('#cd-logo').classList.remove('slide-in');
                  document.querySelector('.cd-btn').classList.remove('slide-in');
                }
                clearTimeout(intervalId);
              }, 50);
            }
          }
          updateSecondaryNavigation();
        });
        //on mobile - open/close secondary navigation clicking/tapping the .cd-secondary-nav-trigger
        angular.element(document.querySelector('.cd-secondary-nav-trigger')).bind('click', function(event) {
          event.preventDefault();
          this.classList.toggle('menu-is-open');
          secondaryNav.querySelector('ul').classList.toggle('is-visible');
        });

        //on mobile - open/close primary navigation clicking/tapping the menu icon
        angular.element(document.querySelector('.cd-primary-nav')).bind('click', function(event) {
          if (event.target.classList.contains('cd-primary-nav')) {
            this.querySelector('ul').classList.toggle('is-visible');
          }
        });
      }
    };
  }
]);